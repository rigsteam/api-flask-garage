#!/usr/bin/python
# coding=utf-8
from flask.ext.api import FlaskAPI
from conf.config import LocalConfig, DevelopConfig
from modules.authentication import authentication, auth
from modules.authors import authors
from modules import mongo
import os

application = FlaskAPI(__name__)

if 'HACKER' in os.environ:
	application.config.from_object(DevelopConfig)
else:
	application.config.from_object(LocalConfig)

## Blueprint
application.register_blueprint(authentication, url_prefix='/authentication')
application.register_blueprint(authors, url_prefix='/authors')

## We are going to start mongo with this application
mongo.init_app(application)

@application.route('/')
@auth.login_required
def index_testing():
	return {'status': 'Ok'}
## End index_testing

if __name__ == "__main__":
    application.run(
    	host=application.config['HOST'],
    	debug=application.config['DEBUG'],
    	port=application.config['PORT']
    )