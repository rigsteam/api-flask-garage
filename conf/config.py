#!/usr/bin/python
# coding=utf-8

class Config(object):
	DEBUG = False
	HOST = '127.0.0.1'
	PORT = 8080	
	SECRET_KEY = '^*d^032DZQX48221d3ZA'

	MONGO_HOST   = 'localhost'
	MONGO_PORT   = '27017'
	MONGO_DBNAME = 'quotes'	
# End Config

class LocalConfig(Config):
	DEBUG = True
## End LocalConfig

class DevelopConfig(Config):	
	HOST       = '0.0.0.0'
	MONGO_HOST = 'ec2-54-153-65-242.us-west-1.compute.amazonaws.com'
	PORT       = 5000
## End DevelopConfig